# Terraform and AWSCLI

[HashiCorp Terraform](https://www.terraform.io/) and [AWS-CLI (v2)](https://aws.amazon.com/cli/) for using inside CI or local user machines.

https://gitlab.com/helecloud/containers/terraform-awscli

## Usage

I would expect that this is mainly used inside CI however, you can use it locally if you want.

Currently this project supports:

1. 0.14.0-rc1
1. 0.13.5 (latest)
1. 0.13.4
1. 0.13.3
1. 0.13.2
1. 0.13.1
1. 0.12.29
1. 0.12.28
1. 0.12.27
1. 0.12.26

### Run locally

```
$~   docker run --rm -it --entrypoint /bin/bash registry.gitlab.com/helecloud/containers/terraform-awscli:latest 
```

### Run inside GitLab CI

```yaml
terraform:
  stage: terraform
  image: registry.gitlab.com/helecloud/containers/terraform-awscli:0.12.28
  script:
    - terraform ... # enter terraform commands
    - aws ... # enter your aws command
```

## Maintainer

* Will Hall
