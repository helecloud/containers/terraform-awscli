ARG TF_VERSION
FROM hashicorp/terraform:$TF_VERSION as terraform

FROM amazonlinux:2

COPY --from=terraform /bin/terraform /bin/terraform

RUN yum -y install zip unzip jq curl python3 && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip -q awscliv2.zip && \
    ./aws/install && \
    aws --version && \
    yum clean all && \
    rm -rf /var/cache/yum
